﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public int score;
    public int rewardCount = 0;

    GameField gameField;

    void Start()
    {
        gameField = FindObjectOfType<GameField>();
        gameField.InitGameField(64, 64);

        int blockCount = 256;

        while(blockCount > 0)
        {
            int rdX = Random.Range(0, 64);
            int rdY = Random.Range(0, 64);
            if (gameField.IsCellBlocked(rdX, rdY))
                continue;

            if (rdX == 1 && rdY == 1)
                continue;

            gameField.BlockCell(rdX, rdY);
            blockCount--;
        }

        gameField.InitUnit(1, 1);


        score = 0;

        GameObject firstReward = gameField.CreateReward(6, 9);

        Vector3 rewardPosition = firstReward.transform.position;
    }

    void Update()
    {
        FindObjectOfType<UIController>().SetScore(score);

        if (rewardCount < 10)
        {
            rewardCount++;
            int rdX = Random.Range(0, 64);
            int rdY = Random.Range(0, 64);

            if (!gameField.IsCellBlocked(rdX, rdY))
                gameField.CreateReward(rdX, rdY);
        }
    }
}
